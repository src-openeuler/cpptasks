Name:           cpptasks
Version:        1.0b5
Release:        1
Summary:        Compile and link task for ant
License:        ASL 2.0
URL:            http://ant-contrib.sourceforge.net/
Source0:        http://downloads.sourceforge.net/ant-contrib/cpptasks-1.0b5.tar.gz
BuildRequires:  maven-local mvn(junit:junit) mvn(org.apache.ant:ant) mvn(xerces:xercesImpl)
BuildArch:      noarch
%description
This ant task can compile various source languages and produce
executables, shared libraries (aka DLL's) and static libraries. Compiler
adaptors are currently available for several C/C++ compilers, FORTRAN,
MIDL and Windows Resource files.

%package        javadoc
Summary:    Javadoc for %{name}
%description    javadoc
Javadoc documentation for %{summary}.

%prep
%setup -q
find . -name "*.jar" -exec rm -f {} \;
find . -name "*.class" -exec rm -f {} \;
sed -i 's/\r//' NOTICE
%pom_remove_plugin :maven-compiler-plugin
%pom_remove_plugin :maven-javadoc-plugin
%pom_remove_plugin :maven-source-plugin
%pom_remove_plugin :maven-assembly-plugin
%pom_change_dep ant:ant org.apache.ant:ant
%pom_remove_dep ant:ant-nodeps
%pom_remove_dep ant:ant-trax
%mvn_file :%{name} ant/%{name}

%build
%mvn_build

%install
%mvn_install
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/ant.d/
echo "ant/%{name}" > $RPM_BUILD_ROOT/%{_sysconfdir}/ant.d/%{name}

%files -f .mfiles
%license LICENSE NOTICE
%doc
%{_sysconfdir}/ant.d/%{name}

%files javadoc -f .mfiles-javadoc
%license LICENSE NOTICE

%changelog
* Mon Aug 3 2020 chengzihan <chengzihan2@huawei.com> - 1.0b5-1
- Package init
